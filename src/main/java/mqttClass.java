/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author mehdi
 */
import static com.oracle.jrockit.jfr.ContentType.Timestamp;
import java.sql.Timestamp;
import java.util.concurrent.CountDownLatch;
import org.eclipse.paho.client.mqttv3.IMqttDeliveryToken;
import org.eclipse.paho.client.mqttv3.MqttCallback;
import org.eclipse.paho.client.mqttv3.MqttClient;
import org.eclipse.paho.client.mqttv3.MqttConnectOptions;
import org.eclipse.paho.client.mqttv3.MqttDeliveryToken;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.eclipse.paho.client.mqttv3.MqttMessage;
import org.eclipse.paho.client.mqttv3.MqttTopic;

public class mqttClass {

    MqttClient myClient;

    static final String BROKER_URL = "m14.cloudmqtt.com:19498";
    static final String M2MIO_DOMAIN = "<Insert m2m.io domain here>";
    static final String M2MIO_STUFF = "things";
    static final String SUB_TOPIC = "temp";
    static final String M2MIO_THING = "Client1";
    static final String M2MIO_USERNAME = "hietjmix";
    static final String M2MIO_PASSWORD_MD5 = "phsGN50Jflk5";

   
    /**
    * @name   connectMqtt
    * @info   connect to assigned broker
    * @author Mehdi
     */
    public void connectMqtt(){
       
        String clientID = M2MIO_THING;
        try {
            myClient = new MqttClient(BROKER_URL, clientID);
            MqttConnectOptions connOpt = new MqttConnectOptions();

            connOpt.setCleanSession(true);
            connOpt.setKeepAliveInterval(30);
            connOpt.setUserName(M2MIO_USERNAME);
            connOpt.setPassword(M2MIO_PASSWORD_MD5.toCharArray());
            
            System.out.println("Connecting to Solace messaging at " + BROKER_URL);
            myClient.connect(connOpt);
        } catch (MqttException e) {
            System.out.println("Error in Connecting to Broker");
            e.printStackTrace();
            System.exit(-1);
        }

        System.out.println("Connected to " + BROKER_URL);
    }

    /**
    * @name   subscriber
    * @info   subscribe to the assigned topic
    * @author Mehdi
     */
    public void subscriber() {
        
        // setup MQTT Client     
        try {       

            // Latch used for synchronizing b/w threads
            final CountDownLatch latch = new CountDownLatch(1);
            
            // Topic filter the client will subscribe to
            final String subTopic = "temp";
            
            // Callback - Anonymous inner-class for receiving messages
            myClient.setCallback(new MqttCallback() {

                public void messageArrived(String topic, MqttMessage message) throws Exception {
                    // Called when a message arrives from the server that
                    // matches any subscription made by the client
                    String time = new Timestamp(System.currentTimeMillis()).toString();
                    System.out.println("\nReceived a Message!" +
                            "\n\tTime:    " + time + 
                            "\n\tTopic:   " + topic + 
                            "\n\tMessage: " + new String(message.getPayload()) + 
                            "\n\tQoS:     " + message.getQos() + "\n");
                    latch.countDown(); // unblock main thread
                }

                public void connectionLost(Throwable cause) {
                    System.out.println("Connection to Solace messaging lost!" + cause.getMessage());
                    latch.countDown();
                }

                public void deliveryComplete(IMqttDeliveryToken token) {
                }
            });
            
            // Subscribe client to the topic filter and a QoS level of 0
            System.out.println("Subscribing client to topic: " + subTopic);
            myClient.subscribe(subTopic, 0);
            System.out.println("Subscribed");

            // Wait for the message to be received
            try {
                latch.await(); // block here until message received, and latch will flip
            } catch (InterruptedException e) {
                System.out.println("I was awoken while waiting");
            }
            
            System.exit(0);
        } catch (MqttException me) {
            System.out.println("reason " + me.getReasonCode());
            System.out.println("msg " + me.getMessage());
            System.out.println("loc " + me.getLocalizedMessage());
            System.out.println("cause " + me.getCause());
            System.out.println("excep " + me);
            me.printStackTrace();
        }
    }
    
    /**
    * @name   publisher
    * @info   publishes the message (content) to the assigned topic
    * @author Mehdi    
    */
    public void publisher(String content) {
        
        String pubTopic = "temp/Sensor";
        try {
            MqttMessage message = new MqttMessage(content.getBytes());
            // Set the QoS on the Messages - 
            // Here we are using QoS of 0 (equivalent to Direct Messaging in Solace)
            message.setQos(0);
            
            System.out.println("Publishing message: " + content);
            
            // Publish the message
            myClient.publish(pubTopic, message);
            
            // Disconnect the client
            myClient.disconnect();
            
            System.out.println("Message published. Exiting");

            System.exit(0);
        } catch (MqttException me) {
            System.out.println("reason " + me.getReasonCode());
            System.out.println("msg " + me.getMessage());
            System.out.println("loc " + me.getLocalizedMessage());
            System.out.println("cause " + me.getCause());
            System.out.println("excep " + me);
            me.printStackTrace();
        }
    }

}
